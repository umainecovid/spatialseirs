yourdate = '3/28/20' #change this to your favorite day this spring

# ==========================================================
# ==========================================================
# dont touch unless you have read the readme and seirs.ipynb
import numpy as np
import pandas as pd
import sys
import gzip
from pyproj import Geod
import random, string

print('imports successful')

us_conf = pd.read_csv('time_series_covid19_confirmed_US.csv',error_bad_lines=False,delimiter=',')
us_dead = pd.read_csv('time_series_covid19_deaths_US.csv',error_bad_lines=False,delimiter=',')
us_conf.keys()
print('data imported')



def uphyper(beta,zeta,sigma,gamma):
    # TODO: @tyler
    sizer = beta.shape[0]
    for i in np.arange(np.random.randint(sizer)):
        a = np.random.randint(sizer)
        b = np.random.randint(sizer)
        beta[a,b] = beta[a,b]*(np.random.random()+0.5) # scale values by 50% to 100%
        zeta[a,b] = zeta[a,b]*(np.random.random()+0.5) # scale values by 50% to 100%
        sigma[a,b] = sigma[a,b]*(np.random.random()+0.5) # scale values by 50% to 100%
        gamma[a,b] = gamma[a,b]*(np.random.random()+0.5) # scale values by 50% to 100%
    return (beta,zeta,sigma,gamma)

def SEIRS(S,E,I,R,N,beta,zeta,sigma,gamma):
    beta,zeta,sigma,gamma = uphyper(beta,zeta,sigma,gamma)
    dSdt = -np.matmul(beta,I)*S/N+np.matmul(zeta,R)
    dEdt = np.matmul(beta,I)*S/N-np.matmul(sigma,E)
    dIdt = np.matmul(sigma,E)-np.matmul(gamma,I)
    dRdt = np.matmul(gamma,I)-np.matmul(zeta,R)
    Sn = S + dSdt
    En = E + dEdt
    In = I + dIdt
    Rn = R + dRdt
    Sn[Sn<0] = 0
    En[En<0] = 0
    In[In<0] = 0
    Rn[Rn<0] = 0
    Sn[Sn>N] = N
    En[En>N] = N
    In[In>N] = N
    Rn[Rn>N] = N
    return (Sn,En,In,Rn,beta,zeta,sigma,gamma)

def progress(count, total, status=''):
    #from: https://gist.github.com/vladignatyev/06860ec2040cb497f0f3
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)

    sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', status))
    sys.stdout.flush()  # As suggested by Rom Ruben (see: http://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console/27871113#comment50529068_27871113)

#Get distance between pairs of lat-lon points
def Distance(lat1,lon1,lat2,lon2):
    wgs84_geod = Geod(ellps='WGS84') #Distance will be measured on this ellipsoid - more accurate than a spherical method
    az12,az21,dist = wgs84_geod.inv(lon1,lat1,lon2,lat2) #Yes, this order is correct
    return dist

print('calculating distances')
distances = np.zeros(((us_conf.shape[0]),(us_conf.shape[0])))
for num1,c1 in enumerate(zip(us_conf['Lat'],us_conf['Long_'])):
    for num2,c2 in enumerate(zip(us_conf['Lat'],us_conf['Long_'])):
        distances[num1,num2] = Distance(c1[0],c1[1],c2[0],c2[1])
    progress(num1,us_conf.shape[0])
print('county to county distances calculated')

proximity = -(distances - distances.max())/distances.max()

try:
  i = 0
  print('running simulation. thanks for you cpu time. press CTRL+C when you want to quit, send us your results folder too')
  while True:
    N = np.random.randint(1e5,size=distances.shape[0])+100
    S = N*(0.9) -  us_conf[yourdate]
    E = N*0.1
    I = us_conf[yourdate]
    R = us_conf[yourdate]*0

    # (S.sum(),E.sum(),I.sum(),R.sum())

    beta_, gamma_, sigma_, zeta_ = np.random.random(4)*100*[3,0.1,1,0.001]

    numcounties = distances.shape[0]
    totalcov = numcounties**2

    # TODO: do something better for these. need to be able to smooth them and set known things (like NYC is problematic etc)
    beta = np.power(proximity,4)/totalcov*beta_ #average numer of contacts per person per day
    zeta = np.random.random((distances.shape))/totalcov*zeta_ # re-susceptible rate 
    sigma = np.random.random((distances.shape))/totalcov*sigma_ # incubation rate
    gamma = np.random.random((distances.shape))/totalcov*gamma_ # rate of recovery

    beta.sum(),gamma.sum(),sigma.sum(),zeta.sum()

    tottime = 100
    timeseries = np.zeros((4,tottime,numcounties))
    for days in np.arange(tottime):
        S,E,I,R,beta,zeta,sigma,gamma = SEIRS(S,E,I,R,N,beta,zeta,sigma,gamma)
        timeseries[:,days,:] = [S,E,I,R] 

    x = ''.join(random.choices(string.ascii_letters + string.digits, k=16))
    f = gzip.GzipFile('results/umcov-'+x+'.npy.gz','w')
    np.save(file=f,arr=timeseries[2:3,:,:])
    f.close()
    i += 1
    print(str(i) +' file(s) saved')
except KeyboardInterrupt:
    pass
